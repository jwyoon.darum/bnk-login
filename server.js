/*jshint esversion: 6 */

var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
const serveStatic = require("serve-static");

let port = process.env.PORT || 3000;

global.__BASEDIR = __dirname + '/';
var util = require(path.join(__BASEDIR, 'util'));

var app = express();

app.use(
    serveStatic(path.join(__dirname, "public"), {
        maxAge: "1m"
    })
);

// Middlewares
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended : true
}));

app.use(function(req, res, next) {
	let pathname = req.url;
    util.log("Request for [" + pathname + "] received.");
	
	if(pathname === "/") {
		res.writeHead(200, { 'Content-Type':'text/html; charset=utf-8' });
		res.write('listening on port:' + port);
		res.end();
		return;
	}

	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
	res.header('Access-Control-Allow-Headers', 'content-type, x-access-token, Set-Cookie');
	next();
});

// API
//app.use('/api/users', require(path.join(__BASEDIR, 'api/users')));
app.use('/api/auth', require(path.join(__BASEDIR, '/api/auth')));

// Server
app.listen(port, function() {
	console.log('listening on port:' + port);
});
